# Homepage

### Previews

- [Large screen](/res/adblockplus.org/screenshots/homepage-large.png)
- [Medium screen](/res/adblockplus.org/screenshots/homepage-tablets.png)
- [Small screen (Android)](/res/adblockplus.org/screenshots/homepage-android.png)

iOS Looks the same as Android (except for the download button text).

#### Hidden sections

- [Wrong browser notification](/res/adblockplus.org/screenshots/wrong-browser-notification.png)
- [French disclaimer](/res/adblockplus.org/screenshots/french-disclaimer.png)
- [Maxthon notification](/res/adblockplus.org/screenshots/maxthon-notification.png)
- [Maxthon featured image](/res/adblockplus.org/screenshots/maxthon-featured-image.png)
- [Internet explorer notification](/res/adblockplus.org/screenshots/internet-explorer-footnote.png)

<!--- 
[Edge featured image](/res/adblockplus.org/screenshots/edge-featured-image.png) 
-->

### Functionality

Each browser supported by Adblock Plus and locale supported by `adblockplus.org` has a separate identical homepage. `adblockplus.org`'s backend redirects requests from `adblockplus.org` to `adblockplus.org/$locale/$browser` appropriately (defaulting to `adblockplus.org/en/chrome`).

The version of the homepage described below is available in the following languages: `EN`, `DE`, `ES`, `FR`, `PT-BR`, `RU`, `ZH-CN`

From now - until the new home page described below is translated into `AR`, `BG`, `HE`, `HU`, `LT`, `NL`, `SK`, `ZH-TW `, the old home page will be served in place of the new home page in those languages respectively.

### Meta data

| Key | Value |
| :-- | :-- |
| title | Adblock Plus &#124; The world's # 1 free ad blocker |
| description | Adblock Plus, the most popular ad blocker on Firefox, Chrome, Safari, Android and iOS. Block pop-ups and annoying ads on websites like Facebook and YouTube. |
| noheading | True |

### Staging area/hero section

```
# SURF THE WEB WITH NO ANNOYING ADS

- Experience a cleaner, faster web and block annoying ads
- Acceptable Ads are allowed by default to support websites ([learn more](acceptable-ads)) [[1]](#footnote)]
- Adblock Plus is free and open source ([GPLv3+](https://www.gnu.org/licenses/gpl.html))

By clicking the Agree and Install button below, you agree to our [Terms of Use](terms).

[Agree and Install for $browser](below)

- [Adblock Plus for Firefox](firefox)
- [Adblock Plus for Chrome](chrome)
- [Adblock Plus for Android](android)
- [Adblock Plus for Opera](opera)
- [Adblock Plus for Internet Explorer](internet-explorer)
- [Adblock Plus for Safari](safari)
- [Adblock Plus for Yandex Browser](yandex-browser)
- [Adblock Plus for Maxthon](maxthon)
- [Adblock Plus for Edge](edge)

[Download Adblock Plus for another browser.](download)
```

The download button is translated as "Install for $browser" in all languages except `English`, `French`, and `German`. The browser is automatically detected on the client side.

#### Desktop download links

| Browser | Download button href |
| :-- | :-- |
| `firefox` | https://eyeo.to/adblockplus/firefox_install/ |
| `chrome` | https://chrome.google.com/webstore/detail/cfhdojbkjhnklbpkdaibdccddilifddb |
| `opera` | https://eyeo.to/adblockplus/opera_install/ |
| `internet-explorer` | https://eyeo.to/adblockplus/ie_install/ |
| `safari` | https://eyeo.to/adblockplus/safari_install/ |
| `yandex` | https://chrome.google.com/webstore/detail/cfhdojbkjhnklbpkdaibdccddilifddb |
| `edge` | https://eyeo.to/adblockplus/edge_install/ |

#### Mobile download links

| Browser | Download button href |
| :-- | :-- |
| `android` | https://eyeo.to/adblockbrowser/android/ |
| `iOS` | https://eyeo.to/adblockplus/ios/ |
| `safari` (iOS) | https://eyeo.to/adblockplus/ios_safari_install/ |
| `samsung-internet` (android) | https://eyeo.to/adblockplus/android_samsung_install/ |

### French disclaimer

This disclaimer is shown only on french language.

See [French disclaimer preview](/res/adblockplus.org/screenshots/french-disclaimer.png).

```
En téléchargeant le logiciel Adblock Plus ou le navigateur Adblock Browser, vous acceptez que si vous utilisez Adblock Plus pour visiter un site Internet en violation de toute obligation ou droit de quelque nature que ce soit en relation avec ce site Internet, en aucun cas EYEO ne pourra être tenu pour responsable envers vous ou tout autre tiers pour toute perte ou dommage (y compris, sans y être limité, les dommages pour perte de chances et perte de bénéfices) découlant directement ou indirectement de votre utilisation de ce logiciel.
```
    
### Maxthon notification

This notification is shown for all browsers except for Maxthon in chinese languages.

See [Maxthon notification preview](/res/adblockplus.org/screenshots/maxthon-notification.png).

```
使用傲游云浏览器，享受最好的广告过滤体验. (内置Adblock Plus) [下载傲游云浏览器](http://www.maxthon.cn)
```

### Internet explorer notification

The following notification appears for users visiting the homepage on Internet Explorer.

See [Internet explorer preview](/res/adblockplus.org/screenshots/internet-explorer-preview.png).

```
Adblock Plus for Internet Explorer is supported by eyeo GmbH on the following editions of Windows 10 – Windows 10 Pro, Windows 10 Education and Windows 10 Enterprise. Adblock Plus for Internet Explorer is supported on the in-market supported servicing branches of Windows 10 including - Current Branch, Current Branch for Business and the following Long-Term Servicing branch: Windows 10.
```

### Video in staging area/hero section

This video is implemented using the [GDPR compatible video](../includes/gdpr-video.md) component.

See preview of [homepage video thumbnail preview](/res/adblockplus.org/screenshots/homepage-video-thumbnail.png). 

| Attribute | Value |
| :-- | :-- |
| URL | https://www.youtube-nocookie.com/embed/pVYtzF5SemU?html5=1&amp;autohide=1&amp;enablejsapi=1&amp;controls=2&amp;fs=1&amp;modestbranding=1&amp;rel=0&amp;showinfo=0&amp;theme=light&amp;autoplay=1 |
| Thumbnail | `[Introduction to Adblock Plus video thumbnail](hero-video.jpg)` |

### Maxthon hidden section

For users visiting the homepage in Maxthon browser, the hero section content is replaced with the contents below and the install button is hidden. 

See [Maxthon featured image preview](/res/adblockplus.org/screenshots/maxthon-featured-image.png).

```
# ADBLOCK PLUS IS ALREADY PART OF YOUR MAXTHON BROWSER

Together, Maxthon and Adblock Plus allow you to experience a faster, cleaner web by eliminating annoying and malicious ads, disabling tracking and more. To help keep the web sustainable, support websites with [Acceptable Ads](https://acceptableads.com/) (enabled by default).
```

### Media mentions section

We have a section under the staging area/hero section where we have a horizontal list with our mentions in the media. See [media mentions preview](/res/adblockplus.org/screenshots/homepage-media-mentions.png).

The following text is placed before the logos of media outlets: ```AS MENTIONED ON:```

For viewports smaller than 1024px in width, the following text is added right below: `(Links open in a new window)`. See media mentions [preview on smaller screens](/res/adblockplus.org/screenshots/homepage-media-mentions-smaller-screens.png).

The logos of media outlets are listed horizontally, the information for each one being as follows:

| Item | Markdown code |
| :--- | :--- |
| Business Insider | `[![Business Insider logo](business-insider.png)](https://www.businessinsider.com/theres-nothing-wrong-about-the-way-adblock-plus-makes-money-2015-9){: title="Business Insider article about Adblock Plus" }` |
| TechCrunch | `[![Tech Crunch article about Adblock Plus](techcrunch.png)](https://techcrunch.com/2016/05/09/adblock-plus-closes-in-on-a-billion-downloads/){: title="Tech Crunch article about Adblock Plus" }` |
| Wall Street Journal | `[![Wall St. Journal logo](wsj.png)](https://www.wsj.com/articles/adblock-plus-chief-till-faida-says-consumers-are-fed-up-with-current-online-ads-1462981668){: title="Wall Street Journal article about Adblock Plus" }` |
| New York Times | `[![New York Times article about Adblock Plus](nyt.png)](https://www.nytimes.com/2015/08/20/technology/personaltech/ad-blockers-and-the-nuisance-at-the-heart-of-the-modern-web.html){: title="New York Times article about Adblock Plus" }` |
| Media Post | `[![Media Post logo](mediapost.png)](http://www.mediapost.com/publications/article/289691/adblock-plus-comes-to-new-york.html){: title="Media Post article about Adblock Plus" }` |

When hovering one of the logos, an overlay appears over the hovered image with an icon that indicates the user the link opens externally. The first logo in the [media mentions preview](/res/adblockplus.org/screenshots/homepage-media-mentions.png) describes how the hover state looks like.


### Key features

The following three key features appear under the media mentions section. See [features preview](/res/adblockplus.org/screenshots/homepage-key-features.png).

```
![Rocket](feature-fast.svg)

## FASTER, MORE ENJOYABLE BROWSING

Block ads that interrupt your browsing experience. Say goodbye to video ads, pop-ups, flashing banners and more. Blocking these annoyances means pages load faster.

![Padlock](feature-safe.svg)

## KEEP YOUR DATA AND DEVICES SAFE

With Adblock Plus avoiding tracking and malware is easy. Blocking intrusive ads reduces the risk of "malvertising" infections. Blocking tracking stops companies following your online activity.

![Checkmark in an octagon](feature-aa.svg)

## NOT ALL ADS ARE BAD

Websites need money to stay free. Support them by allowing Acceptable Ads (enabled by default). Want to hide all ads? No problem. [Learn how](/acceptable-ads#optout)
```

### Adblock Browser section

[Adblock Browser section preview](/res/adblockplus.org/screenshots/adblock-browser-section.png)

[Adblock Browser logo](/res/adblockplus.org/static/abb-logo.png)

```
## WANT TO BLOCK ADS AND DISABLE TRACKING ON MOBILE DEVICES?

![Adblock Browser icon against a shield](abb-logo.png)

### ADBLOCK BROWSER APP

From the team behind Adblock Plus, the most popular ad blocker for desktop browsers, Adblock Browser is now available for your Android and iOS devices.

![Download Adblock Browser on the App Store](apple-app-store-badge.png) {: title="Get Adblock Browser on iOS" }

![Download Adblock Browser on Google Play](google-play-badge.png) {: title="Get Adblock Browser on Android" }
```

### Legal footnote

```
---

[1] The following rules **have** and **always will** apply to everyone, without exception:

- Participants cannot pay to avoid the [criteria](acceptable-ads#criteria-general). Every ad must comply.
- For transparency, we add all Acceptable Ads to our [forum](https://adblockplus.org/forum/viewforum.php?f=12) so that our community of users can provide feedback.
- We listen to our users. If an Acceptable Ads proposal is flagged by our community for a legitimate reason, we will remove it from the whitelist.
- We are able to keep our open source product free by charging large entities a fee for whitelisting services. For the other roughly 90 percent of our partners, these services are offered free of charge.
```

### Cookie notification

The page includes the [cookie notification](/spec/adblockplus.org/includes/cookie-notification.md) as a global widget.
